-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Log = Baik.Base.Log

local BaikItemLevel = _G.BaikItemLevel

-- Create Module
local OptionAction = BaikItemLevel:NewModule("OptionAction")

-- Ace Callbacks
function OptionAction:OnInitialize()
    Log:i("OptionAction OnInitialize")
end

function OptionAction:OnEnable()
    Log:i("OptionAction OnEnable")
end

function OptionAction:OnDisable()
    Log:i("OptionAction OnDisable")
end

-- Export Module
BaikItemLevel.Action.OptionAction = OptionAction
