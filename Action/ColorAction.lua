-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Color = Baik.Base.Color
local Math = Baik.Base.Math
local PlayerAction = Baik.Action.PlayerAction

local BaikItemLevel = _G.BaikItemLevel

-- Create Module
local ColorAction = Class()

-- Constant
local _RANGE = 30
local _MAX_QUALITY = 6

-- Private API
local function _ColorFromQuality(quality)
    local red, green, blue = GetItemQualityColor(quality)

    return Color.New(red, green, blue, 1.0)
end

-- Public API
function ColorAction:GetItemLevelColor(item_level)
    Assert.Number(item_level)
    local player_level = PlayerAction:GetItemLevel()
    local delta_level = Math.Clamp(item_level - player_level, -_RANGE, _RANGE)

    local item_quality = math.floor(_MAX_QUALITY *
                                    (delta_level + _RANGE) / (2 * _RANGE))

    return _ColorFromQuality(item_quality)
end

-- Export Module
BaikItemLevel.Action.ColorAction = ColorAction
