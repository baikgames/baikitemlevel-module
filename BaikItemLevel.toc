## Interface: 90002
## Version: 9.0.2
## Title: BaikItemLevel
## Author: Nikpmup
## Notes: Adds item level to item slots
## Dependencies: BaikCore
## OptionalDeps: Ace3
## X-Embeds: Ace3
BaikItemLevel.lua
Include.xml
