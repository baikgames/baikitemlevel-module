-- Load Module
local _G = _G
local Baik = _G.Baik
local Log = Baik.Base.Log

-- Create Module
local BaikItemLevel = Baik:NewModule("BaikItemLevel")

-- Ace Callbacks
function BaikItemLevel:OnInitialize()
    Log:i("BaikItemLevel OnInitialize")
end

function BaikItemLevel:OnEnable()
    Log:i("BaikItemLevel OnEnable")
end

function BaikItemLevel:OnDisable()
    Log:i("BaikItemLevel OnDisable")
end

-- Export Module
_G.BaikItemLevel = BaikItemLevel
