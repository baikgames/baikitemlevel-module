-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local GameEvent = Baik.Event.GameEvent
local ItemAction = Baik.Action.ItemAction
local Log = Baik.Base.Log
local Table = Baik.Base.Table

local BaikItemLevel = _G.BaikItemLevel
local ColorAction = BaikItemLevel.Action.ColorAction

-- Create Module
local InspectItemFrame = BaikItemLevel:NewModule("InspectItemFrame")

-- Constants
local _INSPECT_ADDON = "Blizzard_InspectUI"

-- Private Method
local function _CreateFrame()
    local frame = CreateFrame("FRAME", "BaikInspectItemFrame", InspectPaperDollItemsFrame)

    frame:SetAllPoints()
    frame:SetFrameStrata("HIGH")

    return frame
end

local function _CreateSlots()
    return {
        [INVSLOT_HEAD] = InspectHeadSlot,
        [INVSLOT_NECK] = InspectNeckSlot,
        [INVSLOT_SHOULDER] = InspectShoulderSlot,
        [INVSLOT_BODY] = InspectShirtSlot,
        [INVSLOT_CHEST] = InspectChestSlot,
        [INVSLOT_WAIST] = InspectWaistSlot,
        [INVSLOT_LEGS] = InspectLegsSlot,
        [INVSLOT_FEET] = InspectFeetSlot,
        [INVSLOT_WRIST] = InspectWristSlot,
        [INVSLOT_HAND] = InspectHandsSlot,
        [INVSLOT_FINGER1] = InspectFinger0Slot,
        [INVSLOT_FINGER2] = InspectFinger1Slot,
        [INVSLOT_TRINKET1] = InspectTrinket0Slot,
        [INVSLOT_TRINKET2] = InspectTrinket1Slot,
        [INVSLOT_BACK] = InspectBackSlot,
        [INVSLOT_MAINHAND] = InspectMainHandSlot,
        [INVSLOT_OFFHAND] = InspectSecondaryHandSlot,
        [INVSLOT_TABARD] = InspectTabardSlot
    }
end

local function _CreateTexts(parent, slots)
    local texts = {}
    Table.ForEach(slots, function(frame, slot)
        local name = string.format("Baik%s", frame:GetName())
        local text = parent:CreateFontString(name, "HIGH", "NumberFontNormal")
        text:SetPoint("TOP", frame, 0, -3)

        texts[slot] = text
    end)

    return texts
end

-- Private API
function InspectItemFrame:_UpdateSlot(slot)
    local text = self._texts[slot]

    -- Check if item exists in slot or is equipable
    local link, id, item_type = ItemAction:InventoryInfo("target", slot)
    if link == nil or not ItemAction:IsEquipment(item_type) then
        text:SetText(nil)
        return
    end

    -- Check if data is fetched from wow servers
    local level = ItemAction:ItemLevel(link)
    if level == nil then
        self._cache[id] = slot
        return
    end

    -- Set Text
    local color = ColorAction:GetItemLevelColor(level)
    text:SetText(level)
    text:SetTextColor(unpack(color))
end

function InspectItemFrame:_UpdateSlots()
    Table.ForEach(self._slots, function(frame, slot)
        self:_UpdateSlot(slot)
    end)
end

function InspectItemFrame:_UpdateId(id, success)
    -- Still waiting for item from server
    if success == nil or success == false then
        return
    end

    -- Item isn't cached
    local slot = self._cache[id]
    if slot == nil then
        return
    end

    self._cache[id] = nil
    self:_UpdateSlot(slot)
end

function InspectItemFrame:_Setup()
    -- Setup Variables
    local frame = _CreateFrame()
    local slots = _CreateSlots()
    local texts = _CreateTexts(frame, slots)
    self._frame = frame
    self._slots = slots
    self._texts = texts
    self._cache = {}

    -- Events
    GameEvent:Get("GET_ITEM_INFO_RECEIVED"):Register(self, InspectItemFrame._UpdateId)
    GameEvent:Get("PLAYER_AVG_ITEM_LEVEL_UPDATE"):Register(self, InspectItemFrame._UpdateSlots)
    GameEvent:Get("INSPECT_READY"):Register(self, InspectItemFrame._UpdateSlots)
end

function InspectItemFrame:_TrySetup(addon)
    if IsAddOnLoaded(_INSPECT_ADDON) then
        self:_Setup()
        GameEvent:Get("ADDON_LOADED"):Unregister(self)
    end
end

-- Ace Callbacks
function InspectItemFrame:OnInitialize()
    if IsAddOnLoaded(_INSPECT_ADDON) then
        self:_Setup()
    else
        GameEvent:Get("ADDON_LOADED"):Register(self, InspectItemFrame._TrySetup)
    end
    Log:i("InspectItemFrame OnInitialize")
end

function InspectItemFrame:OnEnable()
    Log:i("InspectItemFrame OnEnable")
end

function InspectItemFrame:OnDisable()
    Log:i("InspectItemFrame OnDisable")
end

-- Export Module
BaikItemLevel.Frame.InspectItemFrame = InspectItemFrame
