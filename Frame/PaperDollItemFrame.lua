-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local GameEvent = Baik.Event.GameEvent
local ItemAction = Baik.Action.ItemAction
local Log = Baik.Base.Log
local Table = Baik.Base.Table

local BaikItemLevel = _G.BaikItemLevel
local ColorAction = BaikItemLevel.Action.ColorAction

-- Create Module
local PaperDollItemFrame = BaikItemLevel:NewModule("PaperDollItemFrame")

-- Private Method
local function _CreateFrame()
    local frame = CreateFrame("FRAME", "BaikPaperDollItemFrame", PaperDollFrame)

    frame:SetAllPoints()
    frame:SetFrameStrata("HIGH")

    return frame
end

local function _CreateSlots()
    return {
        [INVSLOT_HEAD] = CharacterHeadSlot,
        [INVSLOT_NECK] = CharacterNeckSlot,
        [INVSLOT_SHOULDER] = CharacterShoulderSlot,
        [INVSLOT_BODY] = CharacterShirtSlot,
        [INVSLOT_CHEST] = CharacterChestSlot,
        [INVSLOT_WAIST] = CharacterWaistSlot,
        [INVSLOT_LEGS] = CharacterLegsSlot,
        [INVSLOT_FEET] = CharacterFeetSlot,
        [INVSLOT_WRIST] = CharacterWristSlot,
        [INVSLOT_HAND] = CharacterHandsSlot,
        [INVSLOT_FINGER1] = CharacterFinger0Slot,
        [INVSLOT_FINGER2] = CharacterFinger1Slot,
        [INVSLOT_TRINKET1] = CharacterTrinket0Slot,
        [INVSLOT_TRINKET2] = CharacterTrinket1Slot,
        [INVSLOT_BACK] = CharacterBackSlot,
        [INVSLOT_MAINHAND] = CharacterMainHandSlot,
        [INVSLOT_OFFHAND] = CharacterSecondaryHandSlot,
        [INVSLOT_TABARD] = CharacterTabardSlot
    }
end

local function _CreateTexts(parent, slots)
    local texts = {}
    Table.ForEach(slots, function(frame, slot)
        local name = string.format("Baik%s", frame:GetName())
        local text = parent:CreateFontString(name, "HIGH", "NumberFontNormal")
        text:SetPoint("TOP", frame, 0, -3)

        texts[slot] = text
    end)

    return texts
end

-- Private API
function PaperDollItemFrame:_UpdateSlot(slot)
    local text = self._texts[slot]

    -- Check if item exists in slot or is equipable
    local link, id, item_type = ItemAction:InventoryInfo("player", slot)
    if link == nil or not ItemAction:IsEquipment(item_type) then
        text:SetText(nil)
        return
    end

    -- Check if data is fetched from wow servers
    local level = ItemAction:ItemLevel(link)
    if level == nil then
        self._cache[id] = slot
        return
    end

    -- Set Text
    local color = ColorAction:GetItemLevelColor(level)
    text:SetText(level)
    text:SetTextColor(unpack(color))
end

function PaperDollItemFrame:_UpdateSlots()
    Table.ForEach(self._slots, function(frame, slot)
        self:_UpdateSlot(slot)
    end)
end

function PaperDollItemFrame:_UpdateId(id, success)
    -- Still waiting for item from server
    if success == nil or success == false then
        return
    end

    -- Item isn't cached
    local slot = self._cache[id]
    if slot == nil then
        return
    end

    self._cache[id] = nil
    self:_UpdateSlot(slot)
end

-- Ace Callbacks
function PaperDollItemFrame:OnInitialize()
    -- Setup Variables
    local frame = _CreateFrame()
    local slots = _CreateSlots()
    local texts = _CreateTexts(frame, slots)
    self._frame = frame
    self._slots = slots
    self._texts = texts
    self._cache = {}

    -- Events
    GameEvent:Get("GET_ITEM_INFO_RECEIVED"):Register(self, PaperDollItemFrame._UpdateId)
    GameEvent:Get("PLAYER_AVG_ITEM_LEVEL_UPDATE"):Register(self, PaperDollItemFrame._UpdateSlots)
    Log:i("PaperDollItemFrame OnInitialize")
end

function PaperDollItemFrame:OnEnable()
    self:_UpdateSlots()
    Log:i("PaperDollItemFrame OnEnable")
end

function PaperDollItemFrame:OnDisable()
    Log:i("PaperDollItemFrame OnDisable")
end

-- Export Module
BaikItemLevel.Frame.PaperDollItemFrame = PaperDollItemFrame
