-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Frame = Baik.Base.Frame
local GameEvent = Baik.Event.GameEvent
local ItemAction = Baik.Action.ItemAction
local Log = Baik.Base.Log
local Table = Baik.Base.Table

local BaikItemLevel = _G.BaikItemLevel
local ColorAction = BaikItemLevel.Action.ColorAction

-- Create Module
local BankFrame = BaikItemLevel:NewModule("BankFrame")

-- Constants
local _MIN_BAG = ITEM_INVENTORY_BANK_BAG_OFFSET + 1
local _MAX_BAG = _MIN_BAG + NUM_BANKBAGSLOTS

-- Private Method
local function _CreateFrame(container_name)
    local container = _G[container_name]
    Assert.NotNull(container)

    local frame_name = string.format("Baik%s", container_name)
    local frame = CreateFrame("FRAME", frame_name, container)
    frame:SetAllPoints()
    frame:SetFrameStrata("HIGH")

    return frame, slots
end

local function _CreateSlots(bag_id, container_name, reverse)
    local size = GetContainerNumSlots(bag_id)
    local slots = {}
    for idx = 1, size, 1 do
        local slot_id = reverse and size - idx + 1 or idx
        local slot_name = string.format("%sItem%d",
                                        container_name,
                                        slot_id)
        local slot = _G[slot_name]
        Assert.NotNull(slot)
        slots[idx] = slot
    end

    return slots
end

local function _CreateText(frame, parent)
    local name = string.format("Baik%s", frame:GetName())
    local text = parent:CreateFontString(name, "HIGH", "NumberFontNormal")
    text:SetPoint("TOP", frame, 0, -3)

    return text
end

local function _CreateTexts(parent, slots)
    local texts = {}
    Table.ForEach(slots, function(frame, slot)
        texts[slot] = _CreateText(frame, parent)
    end)

    return texts
end

local function _CreateBag(bag_id)
    local bag = {}
    local frame_name = string.format("ContainerFrame%d", bag_id + 1)
    local frame = _CreateFrame(frame_name)
    local slots = _CreateSlots(bag_id, frame_name, true)
    local texts = _CreateTexts(frame, slots)
    bag.frame = frame
    bag.slots = slots
    bag.texts = texts

    return bag
end

local function _CreateBank()
    local bank = {}
    local frame_name = "BankFrame"
    local frame = _CreateFrame(frame_name)
    local slots = _CreateSlots(BANK_CONTAINER, frame_name, false)
    local texts = _CreateTexts(frame, slots)
    bank.frame = frame
    bank.slots = slots
    bank.texts = texts

    return bank
end

local function _CreateBags()
    local bags = {}
    for bag_id = _MIN_BAG, _MAX_BAG, 1 do
        bags[bag_id] = _CreateBag(bag_id)
    end

    bags[BANK_CONTAINER] = _CreateBank()

    return bags
end

-- Private API
function BankFrame:_UpdateSlot(bag_id, slot)
    local bag = self._bags[bag_id]
    local text = bag.texts[slot]

    -- Check if item exists in slot or is equipable
    local link, id, item_type = ItemAction:ContainerInfo(bag_id, slot)
    if link == nil or not ItemAction:IsEquipment(item_type) then
        text:SetText(nil)
        return
    end

    -- Check if data is fetched from wow servers
    local level = ItemAction:ItemLevel(link)
    if level == nil then
        local cache = {}
        cache.slot = slot
        cache.bag_id = bag_id
        self._cache[id] = cache
        return
    end

    -- Set Text
    local color = ColorAction:GetItemLevelColor(level)
    text:SetText(level)
    text:SetTextColor(unpack(color))
end

function BankFrame:_UpdateSlots(bag_id)
    local bag = self._bags[bag_id]
    Table.ForEach(bag.slots, function(frame, slot)
        self:_UpdateSlot(bag_id, slot)
    end)
end

function BankFrame:_UpdateBags()
    Table.ForEach(self._bags, function(bag, bag_id)
        self:_UpdateSlots(bag_id)
    end)
end

function BankFrame:_RefreshBag(bag_id)
    -- Only handle bank bags
    if bag_id < _MIN_BAG or  bag_id > _MAX_BAG then
        return
    end

    local bag = self._bags[bag_id]

    -- Update slots if bagsize changed
    local size = GetContainerNumSlots(bag_id)
    if size ~= #bag.slots then
        local container_name = string.format("ContainerFrame%d", bag_id + 1)
        local slots = _CreateSlots(bag_id, container_name, true)
        bag.slots = slots

        -- Clear all text
        local texts = bag.texts
        Table.ForEach(texts, function(text)
            text:SetText(nil)
        end)

        -- Update text
        local parent = bag.frame
        Table.ForEach(slots, function(frame, slot)
            -- Create missing text
            local text = texts[slot]
            if text == nil then
                text = _CreateText(frame, parent)
                texts[slot] = text
                return
            end

            -- Update text
            text:ClearAllPoints()
            text:SetPoint("TOP", frame, 0, -3)
        end)
    end
    self:_UpdateSlots(bag_id)
end

function BankFrame:_Setup()
    -- Setup Variables
    self._bags = _CreateBags()
    self._cache = {}
    self:_UpdateBags()

    -- Events
    GameEvent:Get("BAG_UPDATE"):Register(self, BankFrame._RefreshBag)
    GameEvent:Get("PLAYER_AVG_ITEM_LEVEL_UPDATE"):Register(self, BankFrame._UpdateBags)
    GameEvent:Get("PLAYERBANKSLOTS_CHANGED"):Register(function(slot, ...)
        -- Ignore bag slots
        if slot > GetContainerNumSlots(BANK_CONTAINER) then
            return
        end

        self:_UpdateSlot(BANK_CONTAINER, slot)
    end)
    GameEvent:Get("BANKFRAME_OPENED"):Unregister(self)
end

-- Ace Callbacks
function BankFrame:OnInitialize()
    -- Only setup when bank ui is opened
    GameEvent:Get("BANKFRAME_OPENED"):Register(self, BankFrame._Setup)
    Log:i("BankFrame OnInitialize")
end

function BankFrame:OnEnable()
    Log:i("BankFrame OnEnable")
end

function BankFrame:OnDisable()
    Log:i("BankFrame OnDisable")
end

-- Export Module
BaikItemLevel.Frame.BankFrame = BankFrame

